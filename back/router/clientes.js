const { Router } = require('express');
const { Clientes } = require('../schemas/clientes.schemas');


const routerClientes = Router();

routerClientes.get('/:id', (req = request, res = response) => {
    const id = req.params.id;
    res.json(id);
});

routerClientes.get('/', (req = request, res = response) => {
    res.json("Clientes");
});

routerClientes.post('/', (req = request, res = response) => {
    const nuevoCliente = req.body;
    const cliente = new Clientes(nuevoCliente);

});

routerClientes.delete('/:id', (req = request, res = response) => {
    res.json("Clientes");

});

module.exports = { routerClientes };