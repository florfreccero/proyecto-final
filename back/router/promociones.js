const { Router } = require('express');
const { Promociones } = require('../schemas/promociones.schemas');


const routerPromociones = Router();

routerPromociones.get('/:id', (req = request, res = response) => {
    const id = req.params.id;
    res.json(id);
});

routerPromociones.get('/', (req = request, res = response) => {
    res.json("Promociones");
});

routerPromociones.post('/', (req = request, res = response) => {
    // const nuevaPromocion = req.body;
    //const promociones = new Promocion(nuevaPromocion);
    Promociones.insertMany(req.body)
        .then(res.status(201).end())
        .catch(err => {
            console.log(err);
        })
});

routerPromociones.delete('/:id', (req = request, res = response) => {
    res.json("Promociones");

});

module.exports = { routerPromociones };