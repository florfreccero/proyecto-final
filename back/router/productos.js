const { Router } = require('express');
const { Productos } = require('../schemas/productos.schemas');


const routerProductos = Router();

routerProductos.get('/:id', (req = request, res = response) => {
    const id = req.params.id;
    res.json(id);
});

routerProductos.get('/', (req = request, res = response) => {
    res.json("Productos");
});

routerProductos.post('/', (req = request, res = response) => {
    const nuevoProducto = req.body;
    const producto = new Productos(nuevoProducto);
    producto.save()
        .then(res.status(201).json({ "mensaje": "se guardo" }))
        .catch(err => {
            res.status(500).json({ "mensaje": err })
        })
});

routerProductos.delete('/:id', (req = request, res = response) => {
    res.json("Productos");

});

module.exports = { routerProductos };