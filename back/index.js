require(`dotenv`).config();
const express = require('express');
const cors = require(`cors`);
const { dbConnection } = require('./mongo');
const { routerClientes } = require(`./router/clientes`);
const { routerProductos } = require(`./router/productos`);
const { routerPromociones } = require(`./router/promociones`);


const app = express();

const main = () => {
    const PORT = process.env.PORT || 3003;

    app.use(express.json());

    app.use(cors())

    app.use(`/api/clientes`, routerClientes);

    app.use(`/api/productos`, routerProductos);

    app.use(`/api/promociones`, routerPromociones);


    app.use((req, res) => {
        res.status(404).end()
    })

    try {
        app.listen(PORT, () => {
            console.log(`Escuchando en puerto ${PORT}`);
        });
    } catch (error) {
        console.log(`Error!`);
    }
}

dbConnection()
    .then(main)
    .catch(err => {
        console.log(err)
    });