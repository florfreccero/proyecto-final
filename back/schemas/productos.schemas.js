const { Schema, model, Mongoose } = require('mongoose');

const productosSchema = new Schema({
    modelo: {
        type: String,
        require: true,
    },
    material: {
        type: String,
        require: true,
    },
    disenio: {
        type: String,
        require: true,
    },
    precio: {
        type: Number,
        require: true,
    }
});

const Productos = new model(`productos`, productosSchema);

module.exports = { Productos };