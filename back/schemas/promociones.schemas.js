const { Schema, model, Mongoose } = require('mongoose');

const promocionesSchema = new Schema({
    nombre: {
        type: String,
        require: true,
    },
    mes: {
        type: String,
        require: true,
    },
    finalizacion: {
        type: String,
        require: true,
    }
});



const Promociones = new model(`promociones`, promocionesSchema);

module.exports = { Promociones };