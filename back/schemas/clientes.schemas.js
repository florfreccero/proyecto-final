const { Schema, model, Mongoose } = require('mongoose');

const clientesSchema = new Schema({
    nombre: {
        type: String,
        require: true,
    },
    apellido: {
        type: String,
        require: true,
    },
    telefono: {
        type: Number,
        require: true
    }
});



const Clientes = new model(`clientes`, clientesSchema);

module.exports = { Clientes };